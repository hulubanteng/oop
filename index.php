<?php
    //require ('Animal.php');
    require ('Frog.php');
    require ('Ape.php');


    $object = new Animal("shaun");

    echo "Name : ". $object->name. "<br>";
    echo "legs : ". $object ->legs. "<br>";
    echo "cold blooded: ". $object->cold_blooded."<br><br>";
    
    $kodok = new Frog("buduk");
    echo "Name : ". $kodok->name. "<br>";
    echo "legs : ". $kodok->legs. "<br>";
    echo "cold blooded: ". $kodok->cold_blooded."<br>";
    echo "Jump : "; $kodok->jump();
    echo "<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name : ". $sungokong->name. "<br>";
    echo "legs : ". $sungokong->legs. "<br>";
    echo "cold blooded: ". $sungokong->cold_blooded."<br>";
    echo "Yell : "; $sungokong->yell()."<br><br>";